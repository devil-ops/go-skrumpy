# Go Skrumpy

Skrumpy is a CIDR manipulation and information library. Awful with subnetting?
Great with subnetting, but bad at math? Great at subnetting and math, but just
want a command line tool to do it for you? This tool is for you!

## Installation

Binaries can be downloaded from the
[release](https://gitlab.oit.duke.edu/devil-ops/go-skrumpy/-/releases) page,
or you can use the [devil-ops
packaging](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
to install the package.

## Features

By default, all information will be printed out in YAML format. You may change
this by passing any command the `--format` option. This will take most popular
formats, like json, toml, or csv (in some cases).

### Describe one or more CIDRs

Give the `describe` command one or more CIDR addresses, and the description about each will be printed. For example:

```shell
❯ skrumpy describe 192.168.1.0/24 2001:db8:abcd:1234::1/126
- ip: 192.168.1.0
  network: 192.168.1.0
  usable_range: 192.168.1.1-192.168.1.254
  broadcast: 192.168.1.255
  total: 256
  usable: 254
  mask: 255.255.255.0
  wildcard_mask: 0.0.0.255
  cidr_notation: 192.168.1.0/24
  is_private: true
  short: 192.168.1.0/24
- ip: 2001:db8:abcd:1234::1
  network: '2001:db8:abcd:1234::'
  usable_range: 2001:db8:abcd:1234::-2001:db8:abcd:1234::3
  total: 4
  mask: "126"
  cidr_notation: 2001:db8:abcd:1234::1/126
  is_private: false
```

### Check if a given CIDR contains an IP

Use `contains` for this check. If the IP is not contained within the given CIDR,
the system will exit with a non-zero value.

```shell
❯ skrumpy contains 192.168.1.0/24 192.168.1.1
true
❯ skrumpy contains 192.168.1.0/24 192.168.50.1
false
exit status 1
```

### Populate a CIDR to stdout

This will print all IPs in a given CIDR to stdout.

```shell
❯ skrumpy populate 192.168.1.0/30
- 192.168.1.0
- 192.168.1.1
- 192.168.1.2
- 192.168.1.3
```

To do the above, all IPs within the given network must be loaded in to memory
before they are printed out, to ensure proper formatting. This can be less than
ideal for larger networks (Hello IPv6 👋). If you wish to get the IPs as soon as
they are known, pass the `-s` or `--stream` flag in, which will print the IPs
out as they are calculated, in the format of 1 per line. This does not work with
any of the `--format` arguments, however it will get you the start of the IPs
VERY quickly.

```shell
❯ skrumpy populate --stream 2001:db8:abcd:1234::1/64
2001:db8:abcd:1234::1
2001:db8:abcd:1234::2
2001:db8:abcd:1234::3
2001:db8:abcd:1234::4
2001:db8:abcd:1234::5
2001:db8:abcd:1234::6
2001:db8:abcd:1234::7
2001:db8:abcd:1234::8
2001:db8:abcd:1234::9
2001:db8:abcd:1234::a
...
```

### Shrink a CIDR

Given one or more IP ranges, and an optional density percentage, return a list
of high density CIDRs. This is meant to provide a curated list of subnets with a
higher density than the default subnet. Weird right? We use this for things that
have a hard time dealing with sparse subnets. For example, if you have a giant
network, say a /18, but only have IP addresses on the bottom half, this tool
will return a much smaller subnet containing only the active IPs. If the density
requirements cannot be met by a single subnet, multiple subnets will be
returned. This can get the density all the way to 100% per subnet by using
single IP subnets (/32 in IPv4 for example).

Default density is 25%. We start by looking at the largest subnets, and halving
them until we meet the minimum density.

```shell
❯ skrumpy shrink 10.0.0.1 10.0.0.2
- cidr: 10.0.0.0/29
  percent_dense: 25
❯ skrumpy shrink 10.0.0.1 10.0.0.2 -m 50
- cidr: 10.0.0.0/30
  percent_dense: 50
```
