package skrumpy

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"net"
	"net/netip"

	"github.com/gaissmai/extnetip"
)

// PrefixSummary represents a human readable summary of a given cidr
type PrefixSummary struct {
	IP           string  `yaml:"ip,omitempty"`
	Network      string  `yaml:"network,omitempty"`
	UsableRange  string  `yaml:"usable_range,omitempty"`
	Broadcast    string  `yaml:"broadcast,omitempty"`
	Total        float64 `yaml:"total"`
	Usable       int     `yaml:"usable,omitempty"`
	Mask         string  `yaml:"mask,omitempty"`
	WildcardMask string  `yaml:"wildcard_mask,omitempty"`
	CIDRNotation string  `yaml:"cidr_notation,omitempty"`
	IsPrivate    bool    `yaml:"is_private"`
	Short        string  `yaml:"short,omitempty"`
}

// SummarizePrefix returns a PrefixSummary from a given netip.Prefix
func SummarizePrefix(c netip.Prefix) PrefixSummary {
	bits := c.Bits()
	var count float64
	if c.Addr().Is4() {
		count = math.Pow(2, 32-float64(bits))
	} else {
		bits := float64(bits)
		count = math.Pow(2, 128-bits)
	}
	from, last := extnetip.Range(c)
	s := PrefixSummary{
		IP: c.Addr().String(),
		// Network:      from.IPAddr().String(),
		Network:      from.String(),
		Total:        count,
		CIDRNotation: fmt.Sprintf("%v/%v", c.Addr().String(), bits),
		IsPrivate:    c.Addr().IsPrivate(),
	}

	if c.Addr().Is6() {
		s.Mask = fmt.Sprint(bits)
		if bits == 128 {
			s.UsableRange = from.String()
		} else {
			s.UsableRange = fmt.Sprintf("%v-%v", from.String(), last.String())
		}
	}

	// Add some additional bits if ipv4
	if c.Addr().Is4() {
		var usableRange string
		if bits == 32 {
			usableRange = from.String()
		} else {
			usableRange = fmt.Sprintf("%v-%v", from.Next().String(), last.Prev())
		}
		mask := localMask(bits)
		s.Mask = mask
		s.WildcardMask = inverseMask(mask)
		s.Broadcast = last.String()
		s.Usable = max((int(count) - 2), 0)
		s.UsableRange = usableRange
		s.Short = c.String()
	}
	return s
}

func localMask(p int) string {
	// mask := (0xFFFFFFFF << (32 - p)) & 0xFFFFFFFF
	mask := uint32(0xFFFFFFFF << (32 - p))
	var dmask uint64
	dmask = 32
	localmask := make([]uint64, 0, 4)
	var tmp uint64
	for i := 1; i <= 4; i++ {
		tmp = uint64(mask) >> (dmask - 8) & 0xFF
		localmask = append(localmask, tmp)
		dmask -= 8
	}

	return joinMask(localmask)
}

func joinMask(a []uint64) string {
	var buffer bytes.Buffer
	for i := 0; i < len(a); i++ {
		buffer.WriteString(fmt.Sprintf("%v", a[i]))
		if i != len(a)-1 {
			buffer.WriteString(".")
		}
	}

	return buffer.String()
}

func ipv4toUint32(ip string) (uint32, error) {
	i := net.ParseIP(ip)
	if i == nil {
		return 0, errors.New("ParseIP error")
	}
	i = i.To4()
	return binary.BigEndian.Uint32(i), nil
}

func inverseMask(mask string) string {
	i, err := ipv4toUint32(mask)
	if err != nil {
		return ""
	}
	return uint32toIPv4(^i)
}

func uint32toIPv4(ipint uint32) string {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, ipint)
	return ip.String()
}
