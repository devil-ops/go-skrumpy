/*
Package skrumpy (Cider from England or something) does some magic things with CIDRs
*/
package skrumpy

import (
	"errors"
	"fmt"
	"log/slog"
	"math"
	"net"
	"net/netip"
	"os"
	"strings"

	"github.com/projectdiscovery/mapcidr"
)

// Shrinker describes the necessary interface to implement a Shrink
type Shrinker interface {
	Compress(IPSet) (*CompressResponse, error)
}

// Shrink is the thing that takes your IPs, and shrink them down in to some
// smaller CIDRs
type Shrink struct {
	Boundary       *netip.Prefix
	MinPercentFull float64
	logger         *slog.Logger
}

// Ensure this is a valid Shrinker
var _ Shrinker = Shrink{}

// MustNewShrinker returns a new Pomel item, or panics
func MustNewShrinker(options ...func(*Shrink)) *Shrink {
	p, err := NewShrinker(options...)
	panicIfErr(err)
	return p
}

// NewShrinker returns a new Shrink and optional error
func NewShrinker(options ...func(*Shrink)) (*Shrink, error) {
	bp := netip.MustParsePrefix("0.0.0.0/0")
	p := &Shrink{
		MinPercentFull: 25.0,
		Boundary:       &bp,
		logger:         slog.New(slog.NewTextHandler(os.Stderr, nil)),
	}

	// Apply functional options
	for _, opt := range options {
		opt(p)
	}

	return p, nil
}

// ShrinkOpt is a functional option for a Shrink
type ShrinkOpt = func(*Shrink)

// WithLogger is a functional option to set a Logger on a new Shrink
func WithLogger(l *slog.Logger) ShrinkOpt {
	return func(p *Shrink) {
		p.logger = l
	}
}

// WithBoundaryString is a functional option to set the Boundary from a string that is parsable as CIDR
func WithBoundaryString(c string) ShrinkOpt {
	return WithBoundary(netip.MustParsePrefix(c))
}

// WithBoundary is a functional option to set the Boundray from a netip.Prefix
func WithBoundary(c netip.Prefix) ShrinkOpt {
	return func(p *Shrink) {
		p.Boundary = &c
	}
}

// WithMinDensity is a functional option to set the minimum percent full in a new Pommel
func WithMinDensity(m float64) ShrinkOpt {
	return func(p *Shrink) {
		p.MinPercentFull = m
	}
}

// CompressedCIDR holds data about a CIDR that has been compressed to a smaller CIDR, based on the ips within it
type CompressedCIDR struct {
	CIDR         string  `yaml:"cidr"`
	PercentDense float64 `yaml:"percent_dense"`
}

// CompressResponse is the response for a compress job
type CompressResponse struct {
	// Succeeded       bool
	CompressedCIDRs []CompressedCIDR
	pass            int
}

// calculateFullness determines what percentage of a given CIDR is full, using the supplied list of IPs
func calculateFullness(cidr netip.Prefix, ips IPSet) float64 {
	return (ips.lenFloat64() / float64(countIPsWithPrefix(cidr))) * 100
}

func countIPsWithPrefix(p netip.Prefix) int64 {
	return countIPs(p.Bits(), p.Addr().Is6())
}

// countIPs is the full length of the subnet mask
func countIPs(mask int, ip6 bool) int64 {
	if !ip6 {
		return powInt64(2, 32-int64(mask))
	}
	return powInt64(2, 128-int64(mask))
}

func stringsToIPs(s []string) ([]netip.Addr, error) {
	ret := make([]netip.Addr, len(s))
	for idx, item := range s {
		var err error
		ret[idx], err = netip.ParseAddr(strings.TrimSpace(item))
		if err != nil {
			return nil, err
		}
	}
	return ret, nil
}

// ExpandIPStrings given a string, like 192.168.1.0/24 or 192.168.0.0-196.168.0.5,
// return the individual IPs as a string
func ExpandIPStrings(ips string) ([]string, error) {
	switch {
	case strings.Contains(ips, "/"):
		ipn, err := netip.ParsePrefix(ips)
		if err != nil {
			return nil, err
		}
		ips := populatePrefix(ipn)
		ret := make([]string, len(ips))
		for idx, ip := range ips {
			ret[idx] = ip.String()
		}
		return ret, nil

	case strings.Contains(ips, "-"):
		ret := []string{}
		parts := strings.Split(ips, "-")
		if len(parts) != 2 {
			return nil, errors.New("invalid ranged format")
		}
		startEnd, err := stringsToIPs(parts)
		if err != nil {
			return nil, err
		}
		for ip := startEnd[0]; ip.Less(startEnd[1].Next()); ip = ip.Next() {
			ret = append(ret, ip.String())
		}
		return ret, nil
	default:
		possibleIP, err := netip.ParseAddr(ips)
		if err != nil {
			return nil, err
		}
		return []string{possibleIP.String()}, nil
	}
}

func splitCIDRWithPrefix(cidr netip.Prefix, count int) []netip.Prefix {
	splits := splitCIDR(cidr.String(), count)
	if len(splits) == 0 {
		return []netip.Prefix{}
	}
	ret := make([]netip.Prefix, count)
	for idx, item := range splitCIDR(cidr.String(), count) {
		ret[idx] = item
	}
	return ret
}

func splitCIDR(cidr string, count int) []netip.Prefix {
	pre := netip.MustParsePrefix(cidr)
	if pre.Addr().Is4() && (strings.HasSuffix(cidr, "/32")) {
		return []netip.Prefix{}
	}
	if pre.Addr().Is6() && (strings.HasSuffix(cidr, "/128")) {
		return []netip.Prefix{}
	}
	cidrs, err := mapcidr.SplitN(cidr, count)
	panicIfErr(err)
	ret := make([]netip.Prefix, 0, count)
	for _, item := range cidrs {
		if item != nil {
			pitem := ipnet2Prefix(*item)
			ret = append(ret, pitem)
		}
	}
	return ret
}

// CIDRState is the existing state of a CIDR compress job
type CIDRState struct {
	CIDR         string
	RemainingIPs IPSet
	Fullness     float64
}

// Compress takes a CIDR and list of active ips, and returns a list of smaller cidrs it can be compressed to
func (p Shrink) Compress(ips IPSet) (*CompressResponse, error) {
	res := &CompressResponse{}
	log := p.logger.With("parent-cidr", p.Boundary.String())

	// Sanity check, make sure all the IPs we added are within the parent boundary
	for _, ip := range ips.Range() {
		if (p.Boundary != nil) && (!p.Boundary.Contains(ip)) {
			return nil, fmt.Errorf("ip not in parent cidr: %v", ip)
		}
	}

	initFullness := calculateFullness(*p.Boundary, ips)
	log.With("init-fullness", initFullness).Debug("initial Fullness")
	if initFullness >= p.MinPercentFull {
		res.CompressedCIDRs = []CompressedCIDR{
			{
				CIDR:         p.Boundary.String(),
				PercentDense: initFullness,
			},
		}
		// res.Succeeded = true
		log.Debug("initial fullness was enough!")
	} else {
		p.analyzeCIDR(CIDRState{
			CIDR:         p.Boundary.String(),
			RemainingIPs: ips,
		}, res)
	}
	return res, nil
}

// analyzeCIDR recursively scans CIDR networks for the smallest possible set of networks
func (p *Shrink) analyzeCIDR(c CIDRState, cr *CompressResponse) {
	// Increment pass
	// Stop if no remaining ips
	if c.RemainingIPs.Len() == 0 {
		return
	}

	cr.pass++

	for _, section := range splitCIDRWithPrefix(netip.MustParsePrefix(c.CIDR), 2) {
		log := p.logger.With("cidr", c.CIDR).With("sub-cidr", section)
		sectionIPs := c.RemainingIPs.SelectCIDR(section)
		fullNess := calculateFullness(section, sectionIPs)
		log.With("fullness", fullNess).With("section-ips", sectionIPs).Debug("examining")
		// Still need more?
		switch {
		// Do we need to recurse more? Do it here if so
		case fullNess < p.MinPercentFull:
			p.analyzeCIDR(
				CIDRState{
					CIDR:         section.String(),
					Fullness:     fullNess,
					RemainingIPs: sectionIPs,
				},
				cr,
			)
		// Did we get min fullness, but also landed with just a single ip?
		case sectionIPs.Len() == 1:
			cr.CompressedCIDRs = append(cr.CompressedCIDRs, CompressedCIDR{
				CIDR:         sectionIPs.RangeStrings()[0] + ifThenElse(p.Boundary.Addr().Is4(), "/32", "/128").(string),
				PercentDense: 100,
			})
		default:
			cr.CompressedCIDRs = append(cr.CompressedCIDRs, CompressedCIDR{
				CIDR:         section.String(),
				PercentDense: fullNess,
			})
		}
	}
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func powInt64(x, y int64) int64 {
	return int64(math.Pow(float64(x), float64(y)))
}

// convert net.IPNet to netip.Prefix
// from https://djosephsen.github.io//posts/ipnet/
func ipnet2Prefix(ipn net.IPNet) netip.Prefix {
	addr, _ := netip.AddrFromSlice(ipn.IP)
	cidr, _ := ipn.Mask.Size()
	return netip.PrefixFrom(addr, cidr)
}

// ifThenElse evaluates a condition, if true returns the first parameter otherwise the second
func ifThenElse(condition bool, a any, b any) any {
	if condition {
		return a
	}
	return b
}
