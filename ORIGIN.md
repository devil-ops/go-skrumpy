# Origin of this whole thing

I noticed a huge (couple hours) speed up in scanning when I removed
10.184.0.0/16 from the scan list.

That's 65,536 IPs (which tenable would turn into 8,192 chunks).  None of the
IPs in the scan list are in allowed list on the scanners.  I think Tenable was
literally spinning hours creating the 8k chunks, sending them to the scanners,
and processing the null results.

Based on that, I think we should look at dynamically crafting the scan list to
only include ranges with a number of IPs.

If a subnet is half empty, maybe we include an IP range that's smaller than the
full subnet range so we don't scan everything.

If a subnet only has one scannable IP, maybe we only include that IP instead of
the whole subnet.

From what I've been reading, tenable sends scans to the scanners in chunk of 8,
unless the max scans per host is met on that scanner.  I think what this means
is that if a chunk has 1 scannable IP and 7 unscannable IPs, we're going to
waste those 7 slots while waiting for a single scan to run.

I don't think we can get a perfectly crafted list.. but maybe we could shoot
for 25% scannable IPs in every cidr block in the list?

One thing I'm not sure of is if we have a character limit for the scan list

For full clarity, I also removed another /16 from the fuqua space that had
about 1,300 scannable IPs - but since they are different scanners, I think the
timing numbers are still valid

Its all subnet math..  

To get the size of a subnet, you do:  `2**(32-<prefix>)`

Lets go with the common /24..  That would be 2**(32-24) => 2**8 => 256      And
that is the size of a /24

Or for a /16   => 2**(32-16) => 2**16 => 65536

Or a /32   => 2**(32-32) => 2**1 => 1

Going back to our case,  lets say we have a .50 and a .72     A CIDR range that
holds both of those need to hold at least (72 - 50) + 1 => 23  IPs

So, log2(23) => 4.5....    We use ceil() to round up to the nearest INT of 5

We then do 32 - 5 => 27

So, a /27 is the theoretical smallest network that would hold both of those.

This one hits the failure condition I found above..

x.y.z.50/27 is the same as x.y.z.32/27

However, that only includes IPs x.y.z.32 .. x.y.z.63   and it won't include .72

So, we have to do a final check, see that .72 isn't in the new subnet

To fix this, we try x.y.z.50/26  but that only goes from .0 to .63

So we have to go to x.y.z.50/25 (which is x.y.z.0/25)  which goes from .0 to
.127

Having gone through this, I think it is way more complex than yours.  I feel it
might be fasterer as it is a shorter loop at the end, but I don't know that it
is worth it

It is more complex to maintain though

## Update with improvement ideas

My read of SmallestCIDR would give the following results:
X.Y.Z.0/24 (X.Y.Z.5 X.Y.Z.200) -> X.Y.Z.0/24 (no compression)
X.Y.Z.0/24 (X.Y.Z.5 X.Y.Z.120) -> X.Y.Z.0/25 (cut in half)
X.Y.Z.0/24 (X.Y.Z.130 X.Y.X.200) -> X.Y.Z.0/24 (no compression)

That's great.  My suggestion is to take that, then check the fullness of it (as
you do here), but if it doesn't meet the criteria, then split the smallest in
two and start recursing.

With my second sample above, it doesn't meet you min-fullness of 25%, so it
would split X.Y.Z.0/25 into X.Y.Z.0/26 and X.Y.Z.64/26 and repeat on each of
those, combining the results.

One of the things this would also allow is better compression of my third
example.

This expanded algorithm may be overkill, just a possible way to increase
compression if we need it.

## Example Networks

10.148.235.0/24 - Nothing on here but excluded networking devices
10.150.16.64/27 - only has one thing up in here
152.3.69.0/24 - Very sparse
10.183.0.0/18 - Mostly empty
