package skrumpy

import (
	"fmt"
	"net"
	"net/netip"
	"strings"

	"github.com/gaissmai/extnetip"
)

// ipRange is a contiguous set of IPs in the form of first-last
func ipRange(s string) (*IPSet, error) {
	if !isIPRange(s) {
		return nil, fmt.Errorf("not an ip range: %v", s)
	}
	ipss, err := ExpandIPStrings(s)
	if err != nil {
		return nil, err
	}
	return NewIPSet(WithStrings(ipss))
}

// IP is just a single IP
// type IP Expandable

// Domain is a domain that can be translated to IPs by doing a lookup
// type Domain Expandable

// Subnet is a subnet that's expandable
// type Subnet Expandable
func isSubnet(s string) bool {
	return strings.Contains(s, "/")
}

func isIPRange(s string) bool {
	// Is this a X.X.X.X-Y.Y.Y.Y rang thing?
	dashPieces := strings.Split(s, "-")
	if len(dashPieces) == 2 {
		_, err := netip.ParseAddr(dashPieces[0])
		if err != nil {
			return false
		}
		_, err = netip.ParseAddr(dashPieces[0])
		return err == nil
	}
	return false
}

func isDomain(s string) bool {
	// Is this a domain thing?
	_, err := net.LookupIP(s)
	return err == nil
}

// expandableType returns the string representation of an expandable type
func expandableType(s string) (string, error) {
	// I think only subnets have a / in them
	switch {
	case isSubnet(s):
		return "subnet", nil
	case net.ParseIP(s) != nil:
		return "ip", nil
	case isIPRange(s):
		return "ip_range", nil
	case isDomain(s):
		return "domain", nil
	default:
		return "", fmt.Errorf("unknown expandable: %v", s)
	}
}

/*
// DNSToIPer represents like this dns thing i think
type DNSToIPer interface {
	Lookup(s string) []string
}
*/

func mustParsePrefixes(s []string) []netip.Prefix {
	ret := make([]netip.Prefix, len(s))
	for idx, item := range s {
		ret[idx] = netip.MustParsePrefix(item)
	}
	return ret
}

func populatePrefix(p netip.Prefix) []netip.Addr {
	f, _ := extnetip.Range(p)
	ret := []netip.Addr{}
	for ip := f; p.Contains(ip); ip = ip.Next() {
		ret = append(ret, ip)
	}
	return ret
}

func unique[T comparable](s []T) []T {
	inResult := make(map[T]bool)
	var result []T
	for _, str := range s {
		if _, ok := inResult[str]; !ok {
			inResult[str] = true
			result = append(result, str)
		}
	}
	return result
}
