package skrumpy

import (
	"net/netip"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIPListSorting(t *testing.T) {
	got := MustNewIPSet(
		WithStrings([]string{"192.168.1.2", "8.8.8.8", "255.255.255.254", "192.168.1.1", "10.0.0.1"}),
	)
	require.NotNil(t, got)
	require.Equal(t, []string{"8.8.8.8", "10.0.0.1", "192.168.1.1", "192.168.1.2", "255.255.255.254"}, got.RangeStrings())
}

func TestIPListSwap(t *testing.T) {
	l := MustNewIPSet(
		WithStrings([]string{"192.168.1.2", "192.168.1.3"}),
	)
	l.Swap(0, 1)
	require.Equal(
		t,
		l.RangeStrings(),
		[]string{"192.168.1.3", "192.168.1.2"},
	)
}

func TestSelectCIDR(t *testing.T) {
	got := MustNewIPSet(WithStrings([]string{"192.168.1.1", "10.0.0.1"})).SelectCIDR(netip.MustParsePrefix("192.168.1.0/24"))
	require.NotNil(t, got)
	require.Equal(t, []string{"192.168.1.1"}, got.RangeStrings())
}

func TestIPSet(t *testing.T) {
	require.True(
		t,
		netip.MustParseAddr("192.168.1.1").Less(netip.MustParseAddr("192.168.1.2")),
	)
}

func TestNewIPList(t *testing.T) {
	testIPs := []string{
		"192.168.1.1",
		"10.0.0.5",
		"1.2.3.4/32",
	}
	got := MustNewIPSet(
		WithStrings(testIPs),
	)
	require.NotNil(t, got)
	require.Equal(t, len(testIPs), len(got.IPs))
	require.Equal(t, len(testIPs), got.Len())
	require.Equal(t, len(testIPs), len(got.Range()))
	require.IsType(t, []netip.Addr{}, got.Range())
	require.IsType(t, []string{}, got.RangeStrings())
}

func TestUnique(t *testing.T) {
	tests := map[string]struct {
		given  []netip.Addr
		expect []netip.Addr
	}{
		"ipv4": {
			given: []netip.Addr{
				netip.MustParseAddr("192.168.1.1"),
				netip.MustParseAddr("192.168.1.2"),
				netip.MustParseAddr("192.168.1.1"),
			},
			expect: []netip.Addr{
				netip.MustParseAddr("192.168.1.1"),
				netip.MustParseAddr("192.168.1.2"),
			},
		},
	}
	for desc, tt := range tests {
		require.Equal(t, tt.expect, unique(tt.given), desc)
	}
}

func TestMarshalCSV(t *testing.T) {
	given := MustNewIPSet(WithStrings([]string{"1.1.1.1", "2.2.2.2"}))
	got, err := given.MarshalCSV()
	require.NoError(t, err)
	require.Equal(t,
		"1.1.1.1\n2.2.2.2",
		string(got),
	)
}
