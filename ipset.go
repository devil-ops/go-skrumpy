package skrumpy

import (
	"net/netip"
	"sort"
	"strings"
)

// IPSet represents a set of possibly non-contiguous IP addresses. It also
// contains an index by string map
type IPSet struct {
	IPs         []netip.Addr
	stringIndex map[string]int
}

// Append adds new items to the ip listing, and regenerates the index
func (i *IPSet) Append(items ...netip.Addr) {
	i.IPs = append(i.IPs, items...)
	if len(items) > 0 {
		i.reindex()
	}
}

// MarshalCSV converts the ip set to a csv string.
func (i IPSet) MarshalCSV() ([]byte, error) {
	return []byte(strings.Join(i.RangeStrings(), "\n")), nil
}

// SelectCIDR returns only IPs listed that are part of the given CIDR network
func (i IPSet) SelectCIDR(c netip.Prefix) IPSet {
	ret := MustNewIPSet()
	for _, ip := range i.IPs {
		if c.Contains(ip) {
			ret.Append(ip)
		}
	}
	return *ret
}

func (i IPSet) Len() int {
	return len(i.IPs)
}

func (i IPSet) lenFloat64() float64 {
	return float64(len(i.IPs))
}

func (i IPSet) Less(l, j int) bool {
	// return bytes.Compare(i.IPs[l].IP, i.IPs[j].IP) < 0
	return i.IPs[l].Less(i.IPs[j])
}

func (i IPSet) Swap(l, j int) {
	// i.IPs[l].IP, i.IPs[j].IP = i.IPs[j].IP, i.IPs[l].IP
	i.IPs[l], i.IPs[j] = i.IPs[j], i.IPs[l]
}

// Range returns a list of the IPs
func (i IPSet) Range() []netip.Addr {
	return i.IPs
}

// RangeStrings returns the ips as strings
func (i IPSet) RangeStrings() []string {
	ret := make([]string, i.Len())
	for idx, item := range i.IPs {
		ret[idx] = item.String()
	}
	return ret
}

// IPItem represents an IP address, with some accompanying library versions
/*
type IPItem struct {
	Addr netip.Addr
}
*/

// WithStrings is a functionl option to init IPSet using an array of strings
// See this for info on how to make this generic
//
//	https://amirsoleimani.medium.com/functional-options-in-go-with-generic-863dbd68cc6f
func WithStrings(s []string) func(*IPSet) {
	realIPs := []netip.Addr{}
	for _, ip := range s {
		expandedIPs, err := ExpandIPStrings(ip)
		panicIfErr(err)
		for _, expandedIP := range expandedIPs {
			realIPs = append(realIPs, netip.MustParseAddr(strings.TrimSuffix(expandedIP, "/32")))
		}
	}
	// realIPs = removeDuplicateIPItems(realIPs)
	realIPs = unique(realIPs)
	// Sort IPs inline here. We can't do it to the parent type since that's
	// what we are currently creating
	sort.Slice(realIPs, func(i, j int) bool {
		return realIPs[i].Less(realIPs[j])
	})
	// Index where all the ips are
	return func(l *IPSet) {
		l.IPs = realIPs
	}
}

func (i *IPSet) reindex() {
	i.stringIndex = map[string]int{}
	for idx, item := range i.IPs {
		i.stringIndex[item.String()] = idx
	}
}

// MustNewIPSet returns a new instance of IPSet or panics
func MustNewIPSet(options ...func(*IPSet)) *IPSet {
	l, err := NewIPSet(options...)
	panicIfErr(err)
	return l
}

// NewIPSet returns a new instance of IPSet and an optional error
func NewIPSet(options ...func(*IPSet)) (*IPSet, error) {
	l := &IPSet{}
	for _, opt := range options {
		opt(l)
	}
	sort.Sort(l)
	l.reindex()
	return l, nil
}
