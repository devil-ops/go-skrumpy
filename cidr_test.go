package skrumpy

import (
	"net/netip"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPrefixSummary(t *testing.T) {
	require.Equal(
		t,
		PrefixSummary{
			IP:           "192.168.1.0",
			Network:      "192.168.1.0",
			UsableRange:  "192.168.1.1-192.168.1.254",
			Broadcast:    "192.168.1.255",
			CIDRNotation: "192.168.1.0/24",
			Mask:         "255.255.255.0",
			WildcardMask: "0.0.0.255",
			Total:        256,
			Usable:       254,
			IsPrivate:    true,
			Short:        "192.168.1.0/24",
		},
		SummarizePrefix(netip.MustParsePrefix("192.168.1.0/24")),
	)
	require.Equal(
		t,
		PrefixSummary{
			IP:           "192.168.1.1",
			Network:      "192.168.1.1",
			UsableRange:  "192.168.1.1",
			Broadcast:    "192.168.1.1",
			CIDRNotation: "192.168.1.1/32",
			Mask:         "255.255.255.255",
			WildcardMask: "0.0.0.0",
			Total:        1,
			Usable:       0,
			IsPrivate:    true,
			Short:        "192.168.1.1/32",
		},
		SummarizePrefix(netip.MustParsePrefix("192.168.1.1/32")),
	)
	require.Equal(
		t,
		PrefixSummary{
			IP:           "2001:db8:abcd:1234::1",
			Network:      "2001:db8:abcd:1234::",
			UsableRange:  "2001:db8:abcd:1234::-2001:db8:abcd:1234::3",
			CIDRNotation: "2001:db8:abcd:1234::1/126",
			Mask:         "126",
			Total:        4,
			IsPrivate:    false,
		},
		SummarizePrefix(netip.MustParsePrefix("2001:db8:abcd:1234::1/126")),
		"Public IPV6 Network",
	)
	require.Equal(
		t,
		PrefixSummary{
			IP:           "fc00:db8:abcd:1234::1",
			Network:      "fc00:db8:abcd:1234::1",
			UsableRange:  "fc00:db8:abcd:1234::1",
			CIDRNotation: "fc00:db8:abcd:1234::1/128",
			Mask:         "128",
			Total:        1,
			IsPrivate:    true,
		},
		SummarizePrefix(netip.MustParsePrefix("fc00:db8:abcd:1234::1/128")),
		"Single Private IPV6 Network",
	)

	require.Equal(
		t,
		"192.168.1.0",
		SummarizePrefix(netip.MustParsePrefix("192.168.1.2/24")).Network,
		"Test that the correct network comes in when using a non default ip",
	)
	require.Equal(
		t,
		"192.168.1.0",
		SummarizePrefix(netip.MustParsePrefix("192.168.1.2/24")).Network,
		"Test that the correct network comes in when using a non default ip",
	)
}

func TestInverseMask(t *testing.T) {
	// Working
	require.Equal(t, "0.0.0.3", inverseMask("255.255.255.252"))
	// Bogus
	require.Equal(t, "", inverseMask("foo"))
}

/*
func TestNewCIDRWithStrings(t *testing.T) {
	got, err := NewCIDRWithStrings("192.168.1.0/24")
	require.NoError(t, err)
	require.Equal(t, "foo", got)
}
*/

func TestLocalMask(t *testing.T) {
	tests := map[string]struct {
		given  int
		expect string
	}{
		"/32": {given: 32, expect: "255.255.255.255"},
		"/24": {given: 24, expect: "255.255.255.0"},
		"/1":  {given: 1, expect: "128.0.0.0"},
	}
	for desc, tt := range tests {
		got := localMask(tt.given)
		assert.Equal(t, tt.expect, got, desc)
	}
}
