package skrumpy

import (
	"errors"
	"log/slog"
	"net/netip"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestOutOfRange(t *testing.T) {
	pom := MustNewShrinker(
		WithBoundaryString("192.168.1.0/24"),
	)
	got, err := pom.Compress(*MustNewIPSet(WithStrings([]string{"192.168.1.1", "192.168.1.4", "10.0.0.1"})))
	require.Nil(t, got)
	require.Error(t, err)
	require.EqualError(t, err, "ip not in parent cidr: 10.0.0.1")
}

func TestWithLogger(t *testing.T) {
	require.NotNil(t, MustNewShrinker(WithLogger(
		slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{})))))
}

func TestCompressNotNeeded(t *testing.T) {
	pom := MustNewShrinker(
		WithBoundaryString("192.168.1.0/29"),
		WithMinDensity(30.0),
	)
	got, err := pom.Compress(*MustNewIPSet(WithStrings([]string{"192.168.1.1", "192.168.1.4", "192.168.1.5"})))
	require.NoError(t, err)
	require.Equal(
		t,
		&CompressResponse{
			CompressedCIDRs: []CompressedCIDR{{CIDR: "192.168.1.0/29", PercentDense: 37.5}},
		},
		got,
	)
}

func TestExpandIPs(t *testing.T) {
	tests := map[string]struct {
		item      string
		expect    []string
		expectErr string
	}{
		"ranged": {
			item: "192.168.1.1-192.168.1.5",
			expect: []string{
				"192.168.1.1",
				"192.168.1.2",
				"192.168.1.3",
				"192.168.1.4",
				"192.168.1.5",
			},
		},
		"invalid-range": {
			item:      "192.168.1.1-192.168.1.5-",
			expectErr: "invalid ranged format",
		},
		"invalide-range-item": {
			item:      "192.168.1.1-foo",
			expectErr: "ParseAddr(\"foo\"): unable to parse IP",
		},
		"bad-single-item": {
			item:      "foo",
			expectErr: "ParseAddr(\"foo\"): unable to parse IP",
		},
		"cidr": {
			item:   "192.168.1.0/30",
			expect: []string{"192.168.1.0", "192.168.1.1", "192.168.1.2", "192.168.1.3"},
		},
		"bad-cidr": {
			item:      "foo/bar",
			expectErr: "netip.ParsePrefix(\"foo/bar\"): ParseAddr(\"foo\"): unable to parse IP",
		},
		"single-ip6:": {
			item:   "2001:db8:abcd:1234::1",
			expect: []string{"2001:db8:abcd:1234::1"},
		},
	}
	for desc, tt := range tests {
		got, err := ExpandIPStrings(tt.item)
		if tt.expectErr != "" {
			require.Error(t, err)
			require.EqualError(t, err, tt.expectErr)
		} else {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, got, desc)
		}
	}
}

func TestRecurseCIDR(t *testing.T) {
	require.NotPanics(t, func() {
		MustNewShrinker(
			WithMinDensity(25.0),
		).analyzeCIDR(CIDRState{
			CIDR:         "192.168.1.0/27",
			RemainingIPs: *MustNewIPSet(WithStrings([]string{"192.168.1.1", "192.168.1.20"})),
		}, &CompressResponse{})
	})
}

func TestCompress(t *testing.T) {
	tests := map[string]struct {
		cidr     string
		ips      []string
		fullness float64
		expect   *CompressResponse
	}{
		"single-hitters": {
			cidr:     "192.168.1.0/29",
			ips:      []string{"192.168.1.1", "192.168.1.6"},
			fullness: 50.0,
			expect: &CompressResponse{
				CompressedCIDRs: []CompressedCIDR{
					{CIDR: "192.168.1.1/32", PercentDense: 100},
					{CIDR: "192.168.1.6/32", PercentDense: 100},
				},
			},
		},
		"simple": {
			cidr:     "192.168.1.0/29",
			ips:      []string{"192.168.1.1", "192.168.1.2", "192.168.1.6"},
			fullness: 50.0,
			expect: &CompressResponse{
				// Succeeded: true,
				CompressedCIDRs: []CompressedCIDR{
					{CIDR: "192.168.1.0/30", PercentDense: 50},
					{CIDR: "192.168.1.6/32", PercentDense: 100},
				},
			},
		},
		"ipv6": {
			cidr:     "2001:db8:abcd:1234::1/126",
			ips:      []string{"2001:db8:abcd:1234::3"},
			fullness: 50.0,
			expect: &CompressResponse{
				CompressedCIDRs: []CompressedCIDR{
					{CIDR: "2001:db8:abcd:1234::3/128", PercentDense: 100},
				},
			},
		},
	}
	for desc, tt := range tests {
		p := MustNewShrinker(
			WithBoundaryString(tt.cidr),
			WithMinDensity(tt.fullness),
		)
		got, err := p.Compress(*MustNewIPSet(WithStrings(tt.ips)))
		require.NoError(t, err, desc)
		require.Equal(t, tt.expect.CompressedCIDRs, got.CompressedCIDRs, desc)
	}
}

func TestSplitRealCIDR(t *testing.T) {
	tests := map[string]struct {
		cidr   netip.Prefix
		count  int
		expect []netip.Prefix
	}{
		"simple": {
			cidr:   netip.MustParsePrefix("192.168.1.0/24"),
			count:  2,
			expect: mustParsePrefixes([]string{"192.168.1.0/25", "192.168.1.128/25"}),
		},
		"single-v4": {
			cidr:   netip.MustParsePrefix("192.168.1.1/32"),
			count:  2,
			expect: []netip.Prefix{},
		},
		"single-v6": {
			cidr:   netip.MustParsePrefix("2001:db8:3333:4444:5555:6666:7777:8888/128"),
			count:  2,
			expect: []netip.Prefix{},
		},
	}
	for desc, tt := range tests {
		got := splitCIDRWithPrefix(tt.cidr, tt.count)
		require.Equal(t, tt.expect, got, desc)
	}
}

func TestCalculateFullness(t *testing.T) {
	require.Equal(
		t,
		50.0,
		calculateFullness(netip.MustParsePrefix("192.168.1.0/30"), *MustNewIPSet(WithStrings(
			[]string{"192.168.0.1", "192.168.0.2"},
		))),
	)
}

func TestCountIPs(t *testing.T) {
	require.Equal(t, int64(1), countIPs(128, true))
	require.Equal(t, int64(1), countIPs(32, false))
}

func TestPanicIfError(t *testing.T) {
	require.Panics(t, func() { panicIfErr(errors.New("foo")) })
}
