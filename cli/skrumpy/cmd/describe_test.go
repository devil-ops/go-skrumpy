package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDescribe(t *testing.T) {
	out, err := testOut("describe", "192.168.1.0/30")
	require.NoError(t, err)
	require.Equal(
		t,
		"- ip: 192.168.1.0\n  network: 192.168.1.0\n  usable_range: 192.168.1.1-192.168.1.2\n  broadcast: 192.168.1.3\n  total: 4\n  usable: 2\n  mask: 255.255.255.252\n  wildcard_mask: 0.0.0.3\n  cidr_notation: 192.168.1.0/30\n  is_private: true\n  short: 192.168.1.0/30\n",
		out,
	)

	out, err = testOut("describe", "192.168.1.0/24", "2001:db8:abcd:1234::1/126")
	require.NoError(t, err)
	require.Equal(
		t,
		"- ip: 192.168.1.0\n  network: 192.168.1.0\n  usable_range: 192.168.1.1-192.168.1.254\n  broadcast: 192.168.1.255\n  total: 256\n  usable: 254\n  mask: 255.255.255.0\n  wildcard_mask: 0.0.0.255\n  cidr_notation: 192.168.1.0/24\n  is_private: true\n  short: 192.168.1.0/24\n- ip: 2001:db8:abcd:1234::1\n  network: '2001:db8:abcd:1234::'\n  usable_range: 2001:db8:abcd:1234::-2001:db8:abcd:1234::3\n  total: 4\n  mask: \"126\"\n  cidr_notation: 2001:db8:abcd:1234::1/126\n  is_private: false\n",
		out,
	)
}
