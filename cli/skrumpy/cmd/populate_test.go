package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPopulateCmd(t *testing.T) {
	got, err := testOut("populate", "192.168.1.0/30")
	require.NoError(t, err)
	require.Equal(
		t,
		"- 192.168.1.0\n- 192.168.1.1\n- 192.168.1.2\n- 192.168.1.3\n",
		got,
	)

	got, err = testOut("populate", "--stream", "192.168.1.0/30")
	require.NoError(t, err)
	require.Equal(
		t,
		"192.168.1.0\n192.168.1.1\n192.168.1.2\n192.168.1.3\n",
		got,
	)

	got, err = testOut("populate", "--stream", "2001:db8:abcd:1234::1/126")
	require.NoError(t, err)
	require.Equal(
		t,
		"2001:db8:abcd:1234::\n2001:db8:abcd:1234::1\n2001:db8:abcd:1234::2\n2001:db8:abcd:1234::3\n",
		got,
	)
}
