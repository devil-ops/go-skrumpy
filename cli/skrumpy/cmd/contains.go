package cmd

import (
	"errors"
	"net/netip"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

func newContainsCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "contains NETWORK IP",
		Short: "Check if an IP is included in a subnet, exits with non-zero if network does not contain ip",
		Args:  cobra.ExactArgs(2),
		RunE: func(_ *cobra.Command, args []string) error {
			boundary, err := netip.ParsePrefix(args[0])
			if err != nil {
				return err
			}
			ip, err := netip.ParseAddr(args[1])
			if err != nil {
				return err
			}
			res := boundary.Contains(ip)
			if !res {
				return errors.New("ip not contained within network")
			}
			return gout.Print(res)
		},
	}
}
