package cmd

import (
	"net/netip"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-skrumpy"
)

// newDescribeCmd represents the describe command
func newDescribeCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "describe CIDR [CIDR...]",
		Short: "Describe 1 or more CIDRs",
		Args:  cobra.MinimumNArgs(1),
		RunE: func(_ *cobra.Command, args []string) error {
			res := make([]skrumpy.PrefixSummary, len(args))
			for idx, cidrS := range args {
				cidr, err := netip.ParsePrefix(cidrS)
				if err != nil {
					return err
				}
				res[idx] = skrumpy.SummarizePrefix(cidr)
			}
			return gout.Print(res)
		},
	}
}
