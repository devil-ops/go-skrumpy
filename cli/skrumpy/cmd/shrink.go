package cmd

import (
	"net/netip"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/go-skrumpy"
)

func newShrinkCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "shrink IP [IP...]",
		Short: "Return the smallest CIDR(s) containing these IPs",
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			minF, err := cmd.Flags().GetFloat64("min-density")
			if err != nil {
				return err
			}

			boundaryS, err := cmd.Flags().GetString("boundary")
			if err != nil {
				return err
			}

			expandedIPs := []string{}

			for _, possibleIP := range args {
				logger.Debug("starting expansion", "ip", possibleIP)
				expanded, eerr := skrumpy.ExpandIPStrings(possibleIP)
				if eerr != nil {
					return eerr
				}
				expandedIPs = append(expandedIPs, expanded...)
			}

			opts := []skrumpy.ShrinkOpt{
				skrumpy.WithMinDensity(minF),
				skrumpy.WithLogger(logger),
			}
			if boundaryS != "" {
				opts = append(opts, skrumpy.WithBoundary(netip.MustParsePrefix(boundaryS)))
			}

			shrinker, err := skrumpy.NewShrinker(opts...)
			if err != nil {
				return err
			}
			result, err := shrinker.Compress(*skrumpy.MustNewIPSet(skrumpy.WithStrings(expandedIPs)))
			if err != nil {
				return err
			}
			return gout.Print(result.CompressedCIDRs)
		},
	}
	cmd.PersistentFlags().Float64P("min-density", "m", 25.0, "Minimum density of the to be generated CIDR")
	cmd.PersistentFlags().StringP("boundary", "b", "", "This can be used as the initial subnet to begin calculating smaller subnets. Adding this slightly improves performance on IPv4 addresses, but is absolutely required in IPv6, as there are so many IPs")
	return cmd
}

func init() {
}
