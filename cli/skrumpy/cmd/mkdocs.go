package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

// mkdocsCmd represents the mkdocs command
func newMkDocsCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "mkdocs",
		Short: "A brief description of your command",
		Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
		RunE: func(_ *cobra.Command, _ []string) error {
			return doc.GenMarkdownTree(NewRootCmd(), "/tmp/docs")
		},
	}
}
