/*
Package cmd is the command line interface
*/
package cmd

import (
	"fmt"
	"log/slog"
	"os"

	goutbra "github.com/drewstinnett/gout-cobra"
	csv "github.com/drewstinnett/gout-format-csv" // Add CSV to the list of output formats
	"github.com/drewstinnett/gout/v2"
	"github.com/drewstinnett/gout/v2/formats"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	logger  *slog.Logger
	verbose bool
	version = "dev"
)

// NewRootCmd represents the base command when called without any subcommands
func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "skrumpy",
		Long:          paragraph(fmt.Sprintf("\n🍎 %v performs different types of operations on a given %s or %s!", important("Skrumpy"), keyword("CIDR"), keyword("Subnet"))),
		SilenceUsage:  true,
		Version:       version,
		SilenceErrors: true,
		PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
			if err := goutbra.Cmd(cmd); err != nil {
				return err
			}
			gout.SetWriter(cmd.OutOrStdout())
			return nil
		},
	}
	cmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.skrumpy.yaml)")
	cmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose logging")
	if err := goutbra.Bind(cmd); err != nil {
		panic(err)
	}
	cmd.AddCommand(
		newDescribeCmd(),
		newContainsCmd(),
		newPopulateCmd(),
		newShrinkCmd(),
		newMkDocsCmd(),
	)
	return cmd
}

/*
func addSubs(cmd *cobra.Command, subs ...*cobra.Command) {
	for _, sub := range subs {
		cmd.AddCommand(sub)
	}
}
*/

// var rootCmd = NewRootCmd()
var rootCmd *cobra.Command

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd = NewRootCmd()
	// cobra.CheckErr(rootCmd.Execute())
	if err := rootCmd.Execute(); err != nil {
		slog.Warn("fatal error", "error", err)
		os.Exit(2)
	}
}

func init() {
	formats.Add("csv", func() formats.Formatter {
		return csv.Formatter{}
	})
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".skrumpy" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".skrumpy")
	}

	h := &slog.HandlerOptions{}
	if verbose {
		h.Level = slog.LevelDebug
	}
	logger = slog.New(slog.NewTextHandler(os.Stderr, h))

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

/*
func checkErr(err error) {
	if err != nil {
		logger.Error(err.Error())
		os.Exit(2)
	}
}
*/
