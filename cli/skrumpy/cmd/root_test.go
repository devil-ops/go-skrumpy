package cmd

import (
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/spf13/cobra"
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

var testCmd *cobra.Command

func setup() {
	testCmd = NewRootCmd()
}

func testOut(args ...string) (string, error) {
	b := bytes.NewBufferString("")
	testCmd.SetOut(b)
	testCmd.SetArgs(args)
	testCmd.Execute()
	out, err := io.ReadAll(b)
	if err != nil {
		return "", err
	}
	return string(out), err
}

func shutdown() {
}
