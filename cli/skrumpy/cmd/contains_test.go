package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestContainsCmd(t *testing.T) {
	out, err := testOut("contains", "192.168.1.0/24", "192.168.1.1")
	require.NoError(t, err)
	require.Equal(t, "true\n", out)

	out, err = testOut("contains", "192.168.1.0/24", "192.168.2.1")
	require.NoError(t, err)
	require.Equal(t, "", out)
}
