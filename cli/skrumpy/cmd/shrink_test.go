package cmd

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestShrinkCmd(t *testing.T) {
	out, err := testOut("shrink", "192.168.1.1", "192.168.1.100", "192.168.1.100-192.168.1.102")
	require.NoError(t, err)
	require.Equal(
		t,
		"- cidr: 192.168.1.1/32\n  percent_dense: 100\n- cidr: 192.168.1.96/29\n  percent_dense: 37.5\n",
		out,
	)
}
