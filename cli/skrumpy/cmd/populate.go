package cmd

import (
	"fmt"
	"math"
	"net/netip"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// populateCmd represents the populate command
func newPopulateCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "populate CIDR",
		Short: "Given an CIDR, return all IPs contained within",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			c, err := netip.ParsePrefix(args[0])
			if err != nil {
				return err
			}
			stream, err := cmd.Flags().GetBool("stream")
			if err != nil {
				return err
			}

			if !stream {
				var count int64
				if c.Addr().Is4() {
					count = 32 - int64(c.Bits())
				} else {
					count = 128 - int64(c.Bits())
				}
				r := make([]string, int(math.Pow(2, float64(count))))
				var idx int
				for addr := c.Masked().Addr(); c.Masked().Contains(addr); addr = addr.Next() {
					r[idx] = addr.String()
					idx++
				}
				gout.MustPrint(r)
			} else {
				// for addr := c.Addr(); c.Contains(addr); addr = addr.Next() {
				for addr := c.Masked().Addr(); c.Masked().Contains(addr); addr = addr.Next() {
					if _, err := fmt.Fprintln(cmd.OutOrStdout(), addr.String()); err != nil {
						return err
					}
				}
			}
			return nil
		},
	}
	cmd.Flags().BoolP("stream", "s", false, "Stream to stdout instead of printing a preformated list. This is faster to start, but harder to parse for larger CIDRs")
	return cmd
}
