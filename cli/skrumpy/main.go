/*
Package main is the main executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/go-skrumpy/cli/skrumpy/cmd"

func main() {
	cmd.Execute()
}
