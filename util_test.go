package skrumpy

import (
	"net/netip"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestExpandableTypes(t *testing.T) {
	tests := map[string]struct {
		given     string
		expect    string
		expectErr string
	}{
		"subnet":         {given: "192.168.1.0/24", expect: "subnet"},
		"ip":             {given: "192.168.1.5", expect: "ip"},
		"ip-range":       {given: "192.168.1.1-192.168.1.5", expect: "ip_range"},
		"bad-item":       {given: "foo", expectErr: "unknown expandable: foo"},
		"dns for google": {given: "google.com", expect: "domain"},
	}
	for desc, tt := range tests {
		got, err := expandableType(tt.given)
		if tt.expectErr != "" {
			require.Equal(t, "", got, desc)
			require.Error(t, err, desc)
			require.EqualError(t, err, tt.expectErr, desc)
		} else {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, got, desc)
		}
	}
}

func TestMustParsePrefixes(t *testing.T) {
	require.Equal(
		t,
		[]netip.Prefix{
			netip.MustParsePrefix("192.168.1.0/25"),
			netip.MustParsePrefix("192.168.1.128/25"),
		},
		mustParsePrefixes([]string{"192.168.1.0/25", "192.168.1.128/25"}),
	)
}

func TestIPRange(t *testing.T) {
	tests := map[string]struct {
		given     string
		expect    *IPSet
		expectErr string
	}{
		"normal": {
			given:  "192.168.1.1-192.168.1.3",
			expect: MustNewIPSet(WithStrings([]string{"192.168.1.1", "192.168.1.2", "192.168.1.3"})),
		},
		"bad-prefix": {
			given:     "foo-192.168.1.3",
			expectErr: "not an ip range: foo-192.168.1.3",
		},
		"bad-suffix": {
			given:     "192.168.1.1-bar",
			expectErr: "ParseAddr(\"bar\"): unable to parse IP",
		},
		"too-many-dashes": {
			given:     "192.168.1.1-192.168.1.5-",
			expectErr: "not an ip range: 192.168.1.1-192.168.1.5-",
		},
	}
	for desc, tt := range tests {
		got, err := ipRange(tt.given)
		if tt.expectErr != "" {
			require.Nil(t, got, desc)
			require.EqualError(t, err, tt.expectErr, desc)
		} else {
			require.NoError(t, err, desc)
			require.Equal(t, tt.expect, got)
		}
	}
}

func TestPopulate(t *testing.T) {
	require.Equal(
		t,
		[]netip.Addr{
			netip.MustParseAddr("192.168.1.0"),
			netip.MustParseAddr("192.168.1.1"),
			netip.MustParseAddr("192.168.1.2"),
			netip.MustParseAddr("192.168.1.3"),
		},
		populatePrefix(netip.MustParsePrefix("192.168.1.0/30")),
	)
}
